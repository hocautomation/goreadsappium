import com.beust.jcommander.internal.Lists;
import org.testng.TestNG;

import java.io.File;
import java.util.List;

public class ExecutionEngine {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> suites = Lists.newArrayList();
        suites.add(System.getProperty("user.dir") + File.separator + "TestSuite.xml");
        testNG.setTestSuites(suites);
        testNG.run();
    }
}
