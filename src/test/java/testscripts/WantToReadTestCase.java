package testscripts;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

public class WantToReadTestCase {
    public AndroidDriver<MobileElement> driver;
    public WebDriverWait wait;
    public static Properties prop;

    By emailTextbox = By.id("com.goodreads:id/email");
    By passwordTextbox = By.id("com.goodreads:id/password");
    By signinButton = By.id("com.goodreads:id/sign_in_button");
    By loginButton = By.id("com.goodreads:id/login_button");
    By searchText = By.id("com.goodreads:id/action_search");
    By bookTitleText = By.id("com.goodreads:id/book_title");
    By wantToReadText = By.id("com.goodreads:id/wtr_unshelvedStatus");
    By selectedWantToReadText = By.id("com.goodreads:id/wtr_shelvedStatus");
    By searchBox = By.id("com.goodreads:id/search_text");


    @BeforeClass
    public void initTestSuite() throws MalformedURLException {
        loadConfig();
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("deviceName", prop.getProperty("deviceName"));
        caps.setCapability("udid", prop.getProperty("udid")); //DeviceId from "adb devices" command
        caps.setCapability("platformName", prop.getProperty("platformName"));
        caps.setCapability("platformVersion", prop.getProperty("platformVersion"));
        caps.setCapability("skipUnlock", "true");
        caps.setCapability("app", System.getProperty("user.dir") + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "apps" + File.separator + "goodreads.apk");
        caps.setCapability("appPackage", "com.goodreads");
        caps.setCapability("appActivity", "com.goodreads.kindle.ui.activity.SplashActivity");
        caps.setCapability("noReset", "false");
        driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), caps);
        wait = new WebDriverWait(driver, 30);
    }


    @Test
    private void test_that_user_can_add_the_book_to_want_to_read_is_successfully() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(signinButton)).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(emailTextbox)).sendKeys(prop.getProperty("email"));
        wait.until(ExpectedConditions.visibilityOfElementLocated(passwordTextbox)).sendKeys(prop.getProperty("password"));
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginButton)).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(searchText)).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(searchBox)).sendKeys("Sapiens: A Brief History of Humankind");
        wait.until(ExpectedConditions.visibilityOfElementLocated(bookTitleText)).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(wantToReadText)).click();

        String toolBarTitleStr = wait.until(ExpectedConditions.visibilityOfElementLocated(selectedWantToReadText)).getAttribute("content-desc");
        Assert.assertEquals(toolBarTitleStr, "Want to Read, selected");
    }


    @AfterClass
    public void teardown() {
        driver.quit();
    }

    private static void loadConfig() {
        try (InputStream input = new FileInputStream(System.getProperty("user.dir") + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "config.properties")) {
            prop = new Properties();
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
