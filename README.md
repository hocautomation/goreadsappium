# Development Challenge
* This is Automation Test for development challenge using Appium and TestNG.
* Requires: `Appium`, `Maven`
* Support: Android

# Requirement

> Create an automation test using Appium, to test ONE happy path flow on the Goodreads app. The app is available on Play Store here and on the AppStore here.
> The happy path flow should include the following steps:
> 1. A user logging in to the app,
> 2. Searching for a book
> 3. Marking the book as “Want to Read”

# Setup and Run
Make sure to do the following steps to run the script.
* Clone the project 
* Start Appium Server
* Change the config in `config.properties` file for device you want to run Android APP and the account to login Goreads App if you want. 
* You can run the test suite in the class `ExecutionEngine.class`
# Result

![image](https://im.ezgif.com/tmp/ezgif-1-4227c34f50.gif)
